// string reverse 
export function reverseWords(str: string): string {
  return  str.split(" ").reverse().join(" ") 
}
// return string as array
export function stringToArray(s: string): string[] {
	// code code code
  return s.split(" ");
}

/*Given a string s, return true if it is a palindrome, or false otherwise.*/
/*Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.*/
function isPalindrome(s: string): boolean {
  return s.replace(/[^a-z0-9]/gi,'').toLowerCase() == s.split("").reverse().join("").replace(/[^a-z0-9]/gi,'').toLowerCase();
 };
/*Given an integer array nums of length n, you want to create an array ans of 
length 2n where ans[i] == nums[i] and ans[i + n] == nums[i] for 0 <= i < n (0-indexed). */
function getConcatenation(nums: number[]): number[] {
  let res:number[] = [];
  for(let i = 0; i < 2;i++){
      for(let j = 0; j < nums.length;j++){
          res.push(nums[j]);
      }
  }
  return res;
};
/*Given a string s, return the string after replacing every uppercase letter with the same lowercase letter.*/
function toLowerCase(s: string): string {
  return s.toLowerCase();
};
/*Given an integer n, return a string array answer (1-indexed) where:
answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
answer[i] == "Fizz" if i is divisible by 3.
answer[i] == "Buzz" if i is divisible by 5.
answer[i] == i (as a string) if none of the above conditions are true. */
function fizzBuzz(n: number): string[] {
  let res = [];
  for(let i = 1; i <= n;i++) {
      if(i % 3 == 0 && i % 5 == 0) {
          res.push("FizzBuzz");
      } else if(i % 3 == 0) {
          res.push("Fizz");
      } else if(i % 5 == 0) {
          res.push("Buzz");
      } else {
          res.push(i.toString());
      }
  }
  return res;
};
//sqrt root no decimal
function mySqrt(x: number): number {
  return Math.floor(Math.sqrt(x));
};
/*export const rentalCarCost = (d: number): number => d >= 3 && d < 7 ?  d * 40 - 20: d >= 7 ? d * 40 - 50 : d * 40;*/
export const rentalCarCost = (d: number): number => d >= 3 && d < 7 ?  d * 40 - 20: d >= 7 ? d * 40 - 50 : d * 40;
// Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence, which is the number of times you must multiply the digits in num until you reach a single digit.
/*39 --> 3 (because 3*9 = 27, 2*7 = 14, 1*4 = 4 and 4 has only one digit)
999 --> 4 (because 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, and finally 1*2 = 2)
4 --> 0 (because 4 is already a one-digit number)*/
export function persistence(num: number): number {
  // your code here
   if(num.toString().length <= 1) {
     return 0;
   }
    let counter:number = 0;
     let product:number = 1;
 
   while(num.toString().length > 1){
     product = 1;
     for(let i = 0; i < num.toString().length;i++){
       product *= Number(num.toString()[i]);
     }
         num = product;
  
     counter++;
   }
   return counter;
 }
 //print simple square
 export function generateShape(int: number): string {
  let s = "";
   for(let i = 0; i < int;i++)
   {
       for(let j  = 0; j < int;j++){
           s+="+";
       }
     if(i < int-1) {
        s+= "\n";
     }
   }
   return s;
}
// count string where NOT  BETWEEN a || m 
export const  printerError = (s: string): string => [...s].filter(x => x > 'm').length + "/" + s.length;
//sum return string in binary
const sumBinary = (a:number, b:number):string => (a+b).toString(2);
/*Return the number (count) of vowels in the given string.
We will consider a, e, i, o, u as vowels for this Kata (but not y).*/
//str.replace(/[^aeiou]/gi, '').length
export const  getCount = (str: string): number => {
  // @ts-ignore: Object is possibly 'null'.
  return !str.match(/[aeiou]/g) ?  0 : str.match(/[aeiou]/g)?.length;
}
//string end
export const stringEnd = (str: string, ending: string): boolean =>
  str.endsWith(ending);
/*Your task is to write a function which returns the sum of following series upto nth term(parameter).
Series: 1 + 1/4 + 1/7 + 1/10 + 1/13 + 1/16 +...*/
export function SeriesSum(n:number):string
{
  // Happy Coding ^_^
  let result: number = 0;
  for(let i = 0, deler = 1;i  < n;i++, deler += 3) {
    result += 1.00 / deler
  }
  return n != 0 ? result.toFixed(2) : "0.00";
}
/*My grandfather always predicted how old people would get, and right before he passed away he revealed his secret!
In honor of my grandfather's memory we will write a function using his formula!
Take a list of ages when each of your great-grandparent died.
Multiply each number by itself.
Add them all together.
Take the square root of the result.
Divide by two.*/
export function predictAge(age1:number, age2:number, age3:number,age4:number,age5:number,age6:number,age7:number,age8:number): number {
  let q:number = 0;
Array.from(arguments, x => q += x * x);
return Math.floor(Math.sqrt(q)/ 2);
};
/*Spread syntax (...) allows an iterable, such as an array or string, to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected. In an object literal, the spread syntax enumerates 
the properties of an object and adds the key-value pairs to the object being created.*/
/*function predictAge(...ages: number[]): number{
  return Math.floor(Math.sqrt(ages.map(age => age**2).reduce((x, y) => x + y))/2);
};
 */
/*Your goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.
It should remove all values from list a, which are present in list b keeping their order.*/
export const arrayDiff = (a: number[], b: number[]): number[] => a.filter(x => !b.includes(x));
/*You will be given an array and a limit value. You must check that all values in the array are below or equal to the limit value. If they are, return true. Else, return false. */
export function smallEnough(a: number[], limit: number): boolean{
  return a.every(x => x <= limit);
}
/*Write a small function that returns the values of an array that are not odd.
All values in the array will be integers. Return the good values in the order they are given. */
export const noOdds = (values: number[]): number[] => values.filter(n => n % 2 === 0);
/*Welcome. In this kata, you are asked to square every digit of a number and concatenate them.
For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.
Note: The function accepts an integer and returns an integer*/
function squareDigits(num: number): number {
  // may the code be with you
  let temNum = num.toString();
  let result = "";
  for(let i = 0; i < temNum.length;i++)
  {
      result += Math.pow(Number(temNum[i]),2);
  }
  return  parseInt(result);
}
//Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.
const moveZeros = (arr:any[]):any[] => Array.from(arr.filter(x => x!== 0).concat(arr.filter(x => x === 0)));
//Finish the solution so that it sorts the passed in array of numbers. 
//If the function passes in an empty array or null/nil value then it should return an empty array.
export const solutionSortArray = (nums: number[]): number[] =>  nums == null ? [] : nums.sort((a,b) => a - b);
//Check to see if a string has the same amount of 'x's and 'o's. The method must return a boolean and be case insensitive. The string can contain any char.
//str.toLowerCase().split('x').length == str.toLowerCase().split('o').length;
export const xo = (str: string) => Array.from(str.toLowerCase()).filter(x => x == 'o').length == Array.from(str.toLowerCase()).filter(x => x== 'x').length;
//Your task is to sum the differences between consecutive pairs in the array in descending order.
export function sumOfDifferences(arr: number[]): number {
  let result = 0;
  if(arr.length < 2) {
    return 0;
  }
  arr.sort((a,b) => b-a);
  for(let i = 0; i < arr.length-1;i++){
        result += arr[i] - arr[i+1];
  }
  return result;
}
//Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.
const createPhoneNumber = (n:any):string => `(${n[0]}${n[1]}${n[2]}) ${n[3]}${n[4]}${n[5]}-${n[6]}${n[7]}${n[8]}${n[9]}`;
//Complete the solution so that the function will break up camel casing, using a space between words.
//"camelCasing"  =>  "camel Casing"
export function solutionUpperCaseSplitter(string:string):string {
  let result = "";
 Array.from(string).forEach(x => x == x.toUpperCase() ? result += " " + x : result += x);
  return result;
}
/*Write a function named repeater() that takes two arguments (a string and a number), and returns a new string where the input string is repeated that many times.*/
export const repeater = (str: string, n: number): string =>  str.repeat(n); 
/*ATM machines allow 4 or 6 digit PIN codes and PIN codes cannot contain anything but exactly 4 digits or exactly 6 digits.
If the function is passed a valid PIN string, return true, else return false.*/
  export const validatePin = (pin: string): boolean =>  /^\d+$/.test(pin) && (pin.length == 4 || pin.length == 6);
//Your task is to find the nearest square number, nearest_sq(n), of a positive integer n.
export function nearestSq(n:number):number {
  // your code
if(n == 1) return 1;
let sq = n;
let sqMin = n;
while(Math.sqrt(sq) % 1 != 0 && Math.sqrt(sqMin) % 1 != 0) {
  sq++;
  sqMin--;
  if(Math.sqrt(sq) % 1 == 0) {
    return sq;
  }
  if(Math.sqrt(sqMin) % 1 == 0) {
    return sqMin;
  }
}
return 0;
} 
/*Your online store likes to give out coupons for special occasions. Some customers try to cheat the system by entering invalid codes or using expired coupons.
Task
Your mission:
Write a function called checkCoupon which verifies that a coupon code is valid and not expired.
A coupon is no more valid on the day AFTER the expiration date. All dates will be passed as strings in this format: "MONTH DATE, YEAR".*/
export const checkCoupon = (enteredCode: string, correctCode: string, currentDate: string, expirationDate: string): boolean => 
                            enteredCode === correctCode &&  new Date(currentDate) <= new Date(expirationDate);


/*A western man is trying to find gold in a river. To do that, he passes a bucket through the river's soil and then checks if it contains any gold. However, he could be more productive if he wrote an algorithm to do the job for him.
So, you need to check if there is gold in the bucket, and if so, return True/true. If not, return False/false.*/
const checkTheBucket = (bucket)  => bucket.filter(x => x.incudes("gold")).length > 0;

export const a : string = "dev";
export const b : string = "Lab";
export const name : string = a + b;
//Complete the solution so that it returns true if the first argument(string) passed in ends with the 2nd argument (also a string).
export const solution = (str: string, ending: string): boolean => str.endsWith(ending); // TODO: complete

/*There is a bus moving in the city, and it takes and drop some people in each bus stop.
You are provided with a list (or array) of integer pairs. Elements of each pair represent number of people get into bus (The first item) and number of people get off the bus (The second item) in a bus stop.
Your task is to return number of people who are still in the bus after the last bus station (after the last array). Even though it is the last bus stop, the bus is not empty and some people are still in the bus, and they are probably sleeping there :D
Take a look on the test cases.
Please keep in mind that the test cases ensure that the number of people in the bus is always >= 0. So the return integer can't be negative.
The second value in the first integer array is 0, since the bus is empty in the first bus stop.
*/
export function number(busStops: [number, number][]): number {
    // Your Code
      // Good Luck!
    let inbus: number = 0;
    let outbus: number = 0;
    for(let i = 0; i < busStops.length;i++)
    {
         inbus += busStops[i][0];
          outbus +=  busStops[i][1];
    }
    return inbus >= outbus ? inbus - outbus : 0;
  }

  // get even numbers arrayom=
  export const getEvenNumbers = (numbersArray : number[]) : number[] => numbersArray.filter(x => x % 2 == 0);

  //In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
  export class Kata {
    static highAndLow(numbers: string): string {
      // throw new NotImplementedException() ?
      // No, wait, this is TypeScript.
      let numbersSplitted:any = numbers.split(' ').map(x => parseInt(x)).sort((a,b) => a-b);
      return (numbersSplitted[numbersSplitted.length-1] + " " + numbersSplitted[0]);
    }
  }
  export const friend = (friends:string[]) : string[] => friends.filter(x => x.length == 4);

/*The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"]. The second one contains a student's submitted answers.
The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for each incorrect answer, and +0 for each blank answer, represented as an empty string (in C the space character is used).
If the score < 0, return 0.*/
  export function checkExam(array1: string[], array2: string[]): number {
    // good luck 
     let points : number = 0;
     for(let i = 0; i < array1.length;i++){
      array1[i] == array2[i] ? points += 4 : array2[i] == "" ? points += 0 : points--;
     }
     return points < 0 ? 0 : points;
   }
//Count the number of divisors of a positive integer n.
   export function divisors(n: number) {
    // your code here
    let counter : number = 1;
    for(let i = 0; i < n;i++) 
    {
      if(n % i == 0) {
        counter++;
      } 
    }
    return counter;
  }
/*Your team is writing a fancy new text editor and you've been tasked with implementing the line numbering.
Write a function which takes a list of strings and returns each line prepended by the correct number.
The numbering starts at 1. The format is n: string. Notice the colon and space in between.*/
  export function numbering(array: string[]): string[]{
    let arrNumbering: Array<string> = [];
    for(let i = 0; i < array.length;i++) {
     arrNumbering.push( (i+1) + ": " + array[i]);
    }
    return arrNumbering;
    //return array.map((el, i) => `${i+1}: ${el}`);
  }

  /*Complete the function power_of_two/powerOfTwo (or equivalent, depending on your language) that determines if a given non-negative integer is a power of two. From the corresponding Wikipedia entry:
  a power of two is a number of the form 2n where n is an integer, i.e. the result of exponentiation with number two as the base and integer n as the exponent.
  You may assume the input is always valid.*/
  export function isPowerOfTwo(n: number): boolean {
    // Insert magic here
  let isPow : boolean = false;
  if(n == 1)  { return true };
  while(n >  0 && n % 2 == 0) {
    n = n / 2;
    if(n == 1) {
      isPow = true;
    } else {
      isPow = false;
    }
  }
  return isPow;
}
