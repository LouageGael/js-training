//There is an object/class already created called MrFreeze. Mark this object as frozen so that no other changes can be made to it.
Object.freeze(MrFreeze)
/*Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.*/
var twoSum = function(nums, target) {
  for(let i = 0; i < nums.length;i++)
  {
      for(let j = i+1;j  < nums.length;j++){
          if(nums[i] + nums[j] == target) {
              return [i,j];
          }
      }
      
  }
  return [];
};
// Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
var containsDuplicate = function(nums) {
  let b = [];
  for(let i = 0; i < nums.length;i++){
      if(!b.includes(nums[i])){
          b.push(nums[i]);
      } else {
          return true;
      }
  }
  return false;
};
//string to array numbers
function digitize(n) {
  return n.toString().split('').reverse().map(Number);
  }
/*Given a mixed array of number and string representations of integers, add up the string integers and subtract this from the total of the non-string integers.*/
function divCon(x){
  let a = 0;
  let b = 0;
  for(let i = 0; i < x.length;i++){
    if( typeof x[i] === "number") {
      a+=x[i];
    } else if(typeof x[i] === "string") {
      b+=parseInt(x[i]);
    }
  }
  return a - b;
}

function inAscOrder(arr) {
  for(let i = 0; i < arr.length-1;i++)
      {
        if(arr[i] > arr[i+1])
         {
           return false;
         }
     }
      return true;
 }
/*"the-stealth-warrior" gets converted to "theStealthWarrior"
"The_Stealth_Warrior" gets converted to "TheStealthWarrior" */
function toCamelCase(str){
  if(str === "" ||str == null) return "";
  let result = str[0];
  for(let i = 1; i < str.length;i++){
    if(str[i].match(/^[a-zA-Z]+$/)) {
          result += str[i];
    } else 
    {
         result += str[++i].toUpperCase();
    }
  }
  return result;
}
function odds(values){
  // arrow it
  return values.filter( x =>x % 2 !=0 );
}
//Create a function that returns the CSV representation of a two-dimensional numeric array.
function toCsvText(array) {
  // good luck
 let result = "";
 for(let i = 0;i < array.length;i++) {
   for(let j = 0; j < array[i].length;j++) {
       if(j < array[i].length-1){
         result += array[i][j] + ",";
       } else {
         result += array[i][j];
       }
   }
   if(i < array.length-1) {
        result += "\n";
   }

 }
 return result;
}
/*Given any number of arrays each sorted in ascending order, find the nth smallest number of all their elements.
All the arguments except the last will be arrays, the last argument is n. */
function nthSmallest(arrays, n) {
  let tArray = [];
  for(let i = 0; i < arguments.length;i++) {
    for(let j = 0; j < arguments[i].length;j++) {
      tArray.push(arguments[i][j]);
    }
  }
  tArray.sort((a,b) => a-b);
  return arguments.length == 2 ? tArray[n-1] : tArray[arguments[arguments.length-1]-1];
}
//opensenior
const openOrSenior = (data) =>[...data].map(x => x[0]  >= 55 && x[1] >7 ? "Senior" : "Open");
//Write a small function that returns the values of an array that are not odd.
const noOdds = ( values ) => values.filter(x =>  x % 2 == 0);
/*Complete the method which accepts an array of integers, and returns one of the following:
"yes, ascending" - if the numbers in the array are sorted in an ascending order
"yes, descending" - if the numbers in the array are sorted in a descending order
"no" - otherwis
You can assume the array will always be valid, and there will always be one correct answer.*/
const isSortedAndHow = (array)  => JSON.stringify(array) == JSON.stringify(Array.from(array).sort((a,b) => a-b)) ? "yes, ascending" : JSON.stringify(array) == JSON.stringify(Array.from(array).sort((a,b) => b-a)) ? "yes, descending" : "no";
/*Write a function named sumDigits which takes a 
number as input and returns the sum of the absolute 
value of each of the number's decimal digits.*/

   function sumDigits(number) {
    let result = 0;
       console.log(number);
         if(number < 0) number = number * -1;
    for(let i = 0; i < number.toString().length;i++)  {
      result += parseInt(number.toString()[i]);
    }
    return result;
  }
/*Welcome. In this kata, you are asked to square every digit of a number and concatenate them.
For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.*/
//return type of integer
function squareDigits(num){
  const m = Array.from(num.toString()).map(x => x * x);
  let numresult = m[0];
  for(let i = 1; i < m.length;i++) 
  {
        numresult += m[i] + "";
  }
  
  return parseInt(numresult);
}
/*Your team is writing a fancy new text editor and you've been tasked with implementing the line numbering.
Write a function which takes a list of strings and returns each line prepended by the correct number.
The numbering starts at 1. The format is n: string. Notice the colon and space in between.*/
var number=function(array){
  //your awesome code here
  let d = [];
  for(let i= 0 ;i < array.length;i++) 
  {
      d.push((i+1) + ": " + array[i]);
  }
  
  return d;
}
/*Given n, take the sum of the digits of n. 
If that value has more than one digit,
 continue reducing in this way until
 a single-digit number is produced. 
 The input will be a non-negative integer.*/
function digital_root(n) {
  // ...
  let nString = n.toString();
  let sum = 0;
  for(let i = 0; i < nString.length;i++) {
    sum += parseInt(nString[i]);
  }
  let sSum = 0;
  if(sum.toString().length > 1)
  {
    for(let i = 0; i < sum.toString().length;i++) {
          sSum += parseInt(sum.toString()[i]);
     }
     let tSum = 0;
     if(sSum.toString().length > 1) {
        for(let i = 0; i < sSum.toString().length;i++) {
          tSum += parseInt(sSum.toString()[i]);
        }
        return tSum;
     }
     return sSum;
  }
  return sum;
}
/*In a small town the population is p0 = 1000 at the beginning of a year.
 The population regularly increases by 2 percent per year and moreover 50 new inhabitants per year come to live in the town. How many years 
does the town need to see its population greater or equal to p = 1200 inhabitants?*/
function nbYear(p0, percent, aug, p) {
  // your code
  let startYear = p0 + p0 * (percent/100) + aug;
  let counter = 1;
  while(startYear < p)
  {
      startYear =  parseInt(startYear + startYear * (percent/100) + aug);
      counter++;
  }
  return counter;
} 
//Complete the solution so that it returns true if the first argument(string) passed in ends with the 2nd argument (also a string).
const solution = (str, ending) =>  str.endsWith(ending);
//Your task is to make a function that can take any non-negative integer as
// an argument and return it with its digits in descending order. Essentially, rearrange the digits to create the highest possible number.
//   const descendingOrder = (n) =>  parseInt(n.toString().split('').sort().reverse().join(""));
function descendingOrder(n){
  //...
  let res = "";
  let a = n.toString().split('');
  a.sort(function(a, b) {
  return b-a;
});
 for(let i = 0; i < a.length;i++)
 {
     res += a[i];
 }
return parseInt(res);
}
/*Given a string of words, you need to find the highest scoring word.
Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
You need to return the highest scoring word as a string.
If two words score the same, return the word that appears earliest in the original string.
All letters will be lowercase and all inputs will be valid.*/
function high(x){
  let alfa = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
  x = x.toLowerCase();
  let spl = x.split(" ");
  let arrC = [];
  let counter = 0;
  for(let i = 0; i < spl.length;i++) 
  {
      for(let j = 0; j < spl[i].length;j++) 
      {
         for(let z = 0; z < alfa.length; z++) 
         {
              if(spl[i][j] == alfa[z]) 
              {
                  counter += z +1 ;
              }
         }
      }
      arrC.push(counter);
       counter = 0;
  }
 return spl[arrC.indexOf(Math.max(...arrC))];
}
//Find the sum of all multiples of n below m
function sumMul(n,m){
  //your idea here
  let  a = 0;
   if( n < m ) 
   {
    for(let i = 0; i<m;i++) {
      if( i % n == 0) a += i;
    }
     return a;
   }
  
    return "INVALID";
  }
/*You will be given a vector of strings. You must sort it alphabetically 
(case-sensitive, and based on the ASCII values of the chars) and then return the first value.
The returned value must be a string, and have "***" between each of its letters.
You should not remove or add elements from/to the array.*/
/*function twoSort(s) {
  return s.sort()[0].split('').join('***');
}*/
function twoSort(s) {
  let res = "";
  let b = s.sort()[0];
  for(let i = 0; i < b.length;i++)
  {
    if(i != b.length-1) {
          res += b[i] + "***";
    } else {
       res += b[i];
    }
  }

  return res;
}
/*We want to know the index of the vowels in a given word, for example,
 there are two vowels in the word super (the second and fourth letters).
So given a string "super", we should return a list of [2, 4].*/
function vowelIndices(word){
  //your code here
  let vow = ["a","e","i","o","u","y"];
  let arr = [];
  word = word.toLowerCase();
  for(let i = 0; i < word.length;i++)
  {
      for(let j = 0; j < vow.length;j++) {
        if(word[i] == vow[j]) 
        {
                arr.push(i+1);
        }
      }
  }
    return arr;
  }

/*find the remainder*/
 const remainder = (n, m) =>  n > m ? n % m : m % n;
//Your task, is to create NxN multiplication table, of size provided in parameter.
/*1 2 3
2 4 6
3 6 9*/
multiplicationTable = function(size) {
  // insert code here
  let res = [];
  for(let i = 0; i < size;i++) 
  {
    res[i] = [];
    for(let j = 0; j< size;j++) {
        res[i][j]= (i+1) * (j+1);
    }
  }
  return res;
}
/*Given two integers a and b, which can be positive or negative, find the sum of all 
the integers between and including them and return it. If the two numbers are equal return a or b.
Note: a and b are not ordered!*/
function getSum( a,b )
{
  //Good luck!
  let c = 0;
  const max = Math.max(a,b);
  let min = Math.min(a,b);
  for(let i = min; i <= max;i++)
  {
      c+= i;
  }
  return c;
}
//An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a
// function that determines whether a string that contains only letters is an isogram. 
//Assume the empty string is an isogram. Ignore letter case.
function isIsogram(str){
  //...
  str = str.toLowerCase();
  let counter = 0;
  for(let i = 0; i < str.length;i++) 
  {
    for(let j = i+1; j< str.length;j++)
    {
      if(str[i] == str[j]) return false;
    }
  }
  return true;
}
//Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.
// n.substring()
const createPhoneNumber = (n) => `(${n[0]}${n[1]}${n[2]}) ${n[3]}${n[4]}${n[5]}-${n[6]}${n[7]}${n[8]}${n[9]}`;

/*Return an array containing the numbers from 1 to N, where N is the parametered value.
Replace certain values however if any of the following conditions are met:
If the value is a multiple of 3: use the value "Fizz" instead
If the value is a multiple of 5: use the value "Buzz" instead
If the value is a multiple of 3 & 5: use the value "FizzBuzz" instead
N will never be less than 1.*/
function fizzbuzz(n)
{
  let arr = [];
   for(let i =  1; i <= n;i++) {
     if(i % 3 == 0  && i % 5 == 0) {
       arr.push("FizzBuzz");
     } else if(i % 3 == 0) {
       arr.push("Fizz");
     } else if(i % 5 == 0) {
       arr.push("Buzz");
     } else {
       arr.push(i);
     }
   }
  return arr;
}
//In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
function highAndLow(numbers){
  let  q = numbers.split(' ').sort((a,b)=> a-b);
  return  q[q.length-1] + " " +q[0];
 }
//Complete the solution so that the function will break up camel casing, using a space between words.
//const solution = (string)=> string.replace(/[A-Z]/g, x => ' ' + x);
function solution(string) {
  let res = ""   ;
  for(let  i = 0; i < string.length-1;i++)
  {
      res += string[i];
      if(string[i+1] == string[i+1].toUpperCase()) 
      {
          res+= " ";
      }
  }
     res += string[string.length-1];
  return res;
}  
//Write a function that checks if a given string (case insensitive) is a palindrome.
const isPalindrome = (x) => x.split("").reverse().join("").toLowerCase() == x.toLowerCase();
function validateUsr(username) {
  /**
    - `^`        Start from the beginning of the string.
    - `[]`       Allow any character specified, including...
    - `a-z`      anything from a to z,
    - `0-9`      anything from 0 to 9,
    - `_`        and underscore.
    - `{4,16}`   Accept 4 to 16 of allowed characters, both numbers included.
    - `$`        End the string right after specified amount of allowed characters is given.
  */
  const validator = /^[a-z0-9_]{4,16}$/;
  
  return validator.test(username);
}
//Implement a function that accepts 3 integer values a, b, c. The function should return true if a triangle can be built with the sides of given length and false in any other case.
const isTriangle = (a,b,c) => a+b > c && b+c > a && a+c > b;
/*Write a function that takes an array of numbers (integers for the tests) and a target number. It should
 find two different items in the array that, when added together, give the target value. 
 The indices of these items should then be returned in a tuple / list (depending on your language) like so: (index1, index2).*/
function twoSum(numbers, target) {
  // ...
  let resultArray = [];
  for(let i = 0; i < numbers.length;i++) {
    for(let j = i+1;j<numbers.length;j++){
      if(numbers[i]+numbers[j] == target) {
        resultArray.push(i);
        resultArray.push(j);
      }
    }
  }
  return resultArray;
}
/*Given an array of integers, find the one that appears an odd number of times.
There will always be only one integer that appears an odd number of times.*/
function findOdd(A) {
  //happy coding!
  A = A.sort();
  let a = 0;
  let result = 0;
  for(let i = 0 ; i < A.length;i++) {
    if(A[i] ===  A[i+1]) {
      a++;
    } else 
    {
     a++;
      if(a % 2 != 0) 
      {
        
        result = A[i];
          break;
      }
    }
  }
  return result;
}
//Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers. No floats or non-positive integers will be passed.
function sumTwoSmallestNumbers(numbers) {  
  //Code here
  numbers.sort(function(a, b) {
  return a - b;
});
  return numbers[0] + numbers[1];
}
//Count the number of divisors of a positive integer n.
function getDivisorsCnt(n){
    // todo
  let counter =0;
  for(let i = 1; i <= n;i++){
    if(n%i==0){
      counter++;
    }
  }
  return counter;
}
//Write a function that returns both the minimum and maximum number of the given list/array.
function minMax(arr){
  let min = arr[0];
  let max = arr[0];
  let b = [];
  for(let i = 0; i < arr.length;i++){
    if(min > arr[i]) {
      min = arr[i];
    }
    if(max < arr[i]){
      max = arr[i];
    }
  }
  b.push(min);
  b.push(max);
  return b;
}
/*Write a function named sumDigits which 
takes a number as input and returns the sum of the absolute value of each of the number's decimal digits.*/
function sumDigits(number) {
  let num = Math.abs(number).toString();
  let c = 0;
  for(let i = 0; i < num.length;i++){
      if(num[i] != 0){
           c += Number(num[i]);
      }
  }
  return c;
}
/*Return the number (count) of vowels in the given string.
We will consider a, e, i, o, u as vowels for this Kata (but not y).*/
function getCount(str) {
  let vowelsCount = 0;
  // enter your magic here
  for(let i = 0; i < str.length;i++) {
    if(str[i] == 'a'|| str[i] == 'e'|| str[i] == 'i'|| str[i] == 'o'|| str[i] == 'u') {
       vowelsCount++;
       }
  }
  return vowelsCount;
}
/*In this kata you will create a function that takes a list of
 non-negative integers and strings and returns a new list with the strings filtered out.*/
const filter_list = (l) => l.filter(Number.isInteger);
/*Implement a function that adds two numbers together and returns their sum in binary.The conversion can be done before, or after the addition.
The binary number returned should be a string.*/
const addBinary = (a,b) => (a+b).toString(2);
/*Write a function named repeater() that takes two arguments (a string and a number),
 and returns a new string where the input string is repeated that many times.*/
function repeater(string, n){
  //Your code goes here.
  let res = "";
  for(let i = 0; i < n;i++){
    res += string;
  }
  return res;
}
//Complete the function/method so that it returns the url with anything after the anchor (#) removed.
const removeUrlAnchor = (url) =>  url.split("#")[0];
/*An anagram is the result of rearranging the letters of a word to produce a new word (see wikipedia).
Note: anagrams are case insensitive
Complete the function to return true if the two arguments given are anagrams of each other; return false otherwise.*/
var isAnagram = function(test, original) {
  let isAna = false;
   test = test.toLowerCase();
   original = original.toLowerCase();
   let a = [];
   let b = [];
   if(test.length == original.length){
     for(let i = 0 ;i <  test.length;i++) 
     {
       a.push(test[i]);
       b.push(original[i]);
     }
     a.sort();
     b.sort();
     for(let i = 0 ;i <  test.length;i++) 
     {
        if(a[i] == b[i]){
         isAna = true;
       } else{
         isAna = false;
         break;
       }
     }
   }
   return isAna;
 };
 /*Write a function that takes a list of strings as an argument and returns a filtered list containing the same elements but with the 'geese' removed.
The geese are any strings in the following array, which is pre-populated in your solution:*/
function gooseFilter (birds) {
  var geese = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"];
  return birds.filter(x => !geese.includes(x));
};
/*You're laying out a rad pixel art mural to paint on your living room wall in homage to Paul Robertson, your favorite pixel artist.
You want your work to be perfect down to the millimeter. You haven't decided on the dimensions of your piece, how large you want your pixels to be, or which wall you want to use. You just know that you want to fit an exact number of pixels.
To help decide those things you've decided to write a function, is_divisible() that will tell you whether a wall of a certain length can exactly fit an integer number of pixels of a certain length.
Your function should take two arguments: the size of the wall in millimeters and the size of a pixel in millimeters. It should return True if you can fit an exact number of pixels on the wall, otherwise it should return False. For example is_divisible(4050, 27) should return
 True, but is_divisible(4066, 27) should return False.*/
const isDivisible = (wallLength, pixelSize) => wallLength % pixelSize == 0 ? true : false;

//Your goal is to return multiplication table for number that is always an integer from 1 to 10.
function multiTable(number) {
  // good luck
  let result = "";
  for(let i = 1 ; i <= 10;i++) 
  {
    if(i < 10) 
    {
      result += i + " * " + number +" = "  + (i * number)  + "\n";
    } else 
    {
       result += i + " * " + number +" = "  + (i * number);
    }
  }
  
  return result;
}   
/*The male gametes or sperm cells in humans and other mammals are heterogametic and contain one of two types of sex chromosomes. They are either X or Y. The female gametes or eggs however, contain only the X sex chromosome and are homogametic.

The sperm cell determines the sex of an individual in this case. If a sperm cell containing an X chromosome fertilizes an egg, the resulting zygote will be XX or female. If the sperm cell contains a Y chromosome, then the resulting zygote will be XY or male.

Determine if the sex of the offspring will be male or female based on the X or Y chromosome present in the male's sperm.

If the sperm contains the X chromosome, return "Congratulations! You're going to have a daughter."; If the sperm contains the Y chromosome, return "Congratulations! You're going to have a son."; */
const chromosomeCheck = (sperm) => sperm == "XY" ? "Congratulations! You're going to have a son." : "Congratulations! You're going to have a daughter.";


//Given a month as an integer from 1 to 12, return to which quarter of the year it belongs as an integer number.
//For example: month 2 (February), is part of the first quarter; month 6 (June), is part of the second quarter; and month 11 (November), is part of the fourth quarter.
const quarterOf = (month) => month <= 3 ? 1 : month > 3 && month <= 6 ? 2 : month > 6 && month <= 9 ? 3 : 4;


/*Your task is to write function findSum.
Upto and including n, this function will 
return the sum of all multiples of 3 and 5.*/
function findSum(n) {
  let sum = 0;
  for(let i = 0; i <= n; i++) 
  {
      if(i % 3 == 0 || i % 5 == 0)  {
        sum += i;
      }
  }
  return sum;
}  
/*Our fruit guy has a bag of fruit (represented as an array of strings) where some fruits are rotten. He wants to replace all the rotten pieces of fruit with fresh ones. For example, given ["apple","rottenBanana","apple"] the replaced array should be ["apple","banana","apple"]. Your task is to implement a method that accepts an array of strings containing fruits should returns an array of strings where all the rotten fruits are replaced by good ones.*/
function removeRotten(bagOfFruits){
  // ...
  if(bagOfFruits == null || bagOfFruits == []) return [];
  let a = [];
  for(let i = 0; i < bagOfFruits.length;i++) {
    if(bagOfFruits[i].startsWith("rotten")) {
      let sp = bagOfFruits[i].split("rotten");
        a.push(sp[1].toLowerCase());
    } else {
      a.push(bagOfFruits[i]);
    }
  }
  return a;
}
//You will be given an array and a limit value. You must check that all values in the array are below or equal to the limit value. If they are, return true. Else, return false.
function smallEnough(a, limit){
   return a.filter(x => x <= limit).length == a.length;
}
//get unique num in array

function findUniq(arr) {
  return arr.find(e => arr.lastIndexOf(e) === arr.indexOf(e));
}

/*Your task is to make a function that can take any non-negative integer as an argument and return it with its digits in descending order. Essentially, rearrange the digits to create the highest possible number.*/
function descendingOrder(n){
  //...
  return  Number(Array.from(n.toString()).sort((a,b)=> b-a).join(""));
}